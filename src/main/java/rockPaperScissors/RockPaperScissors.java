package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        

        String keepplaying;
        do {
            System.out.print("Let's play round " + roundCounter + "\n");
            boolean validChoice = false;
            String human_Choice;
                do { 
                    System.out.print("Your choice (Rock/Paper/Scissors)?");
                 human_Choice = readInput("");

                    if (rpsChoices.contains(human_Choice)){
                    validChoice = true;
                    }else {
                        System.out.print("I do not understand " + human_Choice + ". Could you try again?\n");
                    }
                } while (!validChoice);
            

            
               
                  String AI_Choice = "";
                    int randomnumber; 
                    Double randNum = Math.random()*3;
                    randomnumber = randNum.intValue();
                    
                    if (randomnumber == 0) {
                        AI_Choice = "rock";
                    }else if (randomnumber == 1) {
                        AI_Choice = "paper";
                    }else {
                    AI_Choice= "scissors";
                    }
                    
    
    
                    String winner;
                    if(human_Choice.equals(AI_Choice)) {
                         winner = ("It's a tie!");
                    } else if ((human_Choice.equals("rock") && AI_Choice.equals("scissors")) || (human_Choice.equals ("scissors")
                    && AI_Choice.equals("paper")) || (human_Choice.equals("paper") && AI_Choice.equals("rock"))) {
                         winner =("Human wins!");
                        humanScore++;
                    } else {
                         winner = ("Computer wins!");
                        computerScore++;
                    }


                    System.out.print("Human chose " + human_Choice + ", computer chose " + AI_Choice + ". " + winner + "\n");
                    System.out.print("Score: human " + humanScore + ", computer " + computerScore + "\n");
                    System.out.print("Do you wish to continue playing? (y/n)?");

                    keepplaying = readInput("");
                     roundCounter++; 
    
                
        } while (keepplaying.equals("y"));

        System.out.print("Bye bye :)");
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
